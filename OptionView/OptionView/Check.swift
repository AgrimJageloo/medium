//
//  Check.swift
//  OptionView
//
//  Created by Poonam Yadav on 09/10/19.
//  Copyright © 2019 CustomAppDelegate. All rights reserved.
//

import UIKit

class ClassName {
     //1- contain all stored public properties in dictionary order
    //2- contain all stored private properties in dictionary order
    // 3- initialiser
}

// extension for constnats
extension ClassName {
    //constant1..
    //constant2..
    //constant3..
    //.
    //.
    //constant n
}

// extension for private methods and private computed properties except overriden or protocol methods
extension ClassName {
    // private method1
    // private method2
    //.
    //.
    //.
    // private methodn
}

// extension for public methods and public computed properties
extension ClassName {
    
}

// extension for overriden method or life cycle method
extension ClassName {
    
}

// extension for protocol methos
extension ClassName {
    
}
