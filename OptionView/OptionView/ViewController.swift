//
//  ViewController.swift
//  OptionView
//
//  Created by Poonam Yadav on 09/10/19.
//  Copyright © 2019 CustomAppDelegate. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    
    // MARK: - jageloo
    override func viewDidLoad() {
        super.viewDidLoad()
        let option = OptionButtonView(title: "Check Digit")
        self.containerView.addSubview(option, with: .zero)
        self.containerView.layoutIfNeeded()
    }
}
