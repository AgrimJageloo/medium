//
//  BMBSearchCriteriaView.swift
//  BMBSearchCriteriaView
//
//  Created by Poonam Yadav on 09/10/19.
//  Copyright © 2019 CustomAppDelegate. All rights reserved.
//

import UIKit

extension OptionButtonView {
    private static let spacing: CGFloat = 8.0
    private static let titlePading = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    private static let iconPading = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 10)
    private static let borderWidth: CGFloat = 1.0
    private static let cornerRadius: CGFloat = 4.0
    private static let fontSize: CGFloat = 14
    private static let iconName = "cross"
}

class OptionButtonView: UIView {
    
    private var title: String
    
    private lazy var actionButton: UIButton = { button in
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(self.selectOption), for: .touchUpInside)
        return button
    }(UIButton())
    
    private lazy var titleView: UIView = { view in
        let selfType = type(of: self)
        let label: UILabel = { label in
            label.text = self.title
            label.font = UIFont.systemFont(ofSize: selfType.fontSize)
            return label
        }(UILabel())
        view.addSubview(label, with: selfType.titlePading)
        return view
    }(UIView())
    
    private lazy var iconView: UIView = { view in
        let selfType = type(of: self)
        let closeIconImageView: UIImageView = { imageView in
            imageView.image = UIImage(named: selfType.iconName)
            return imageView
        }(UIImageView())
        view.addSubview(closeIconImageView, with: selfType.iconPading)
        return view
    }(UIView())
    
    init(title: String) {
        self.title = title
        super.init(frame: .zero)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.title = ""
        super.init(coder: aDecoder)
    }
}

extension OptionButtonView {
    
    private func setup() {
        let selfType = type(of: self)
        let stack: UIStackView = { stackView in
            stackView.axis = .horizontal
            stackView.distribution = .fill
            stackView.spacing = selfType.spacing
            return stackView
        }(UIStackView())
        stack.addArrangedSubview(titleView)
        stack.addArrangedSubview(iconView)
        self.addSubview(stack, with: .zero)
        self.layer.borderWidth = selfType.borderWidth
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.cornerRadius = selfType.cornerRadius
        self.addSubview(self.actionButton, with: .zero)
        self.updateUI()
    }
    
    @objc private func selectOption() {
        self.actionButton.isSelected = !self.actionButton.isSelected
        self.updateUI()
    }
    
    private func updateUI() {
        self.iconView.isHidden = !self.actionButton.isSelected
        self.backgroundColor = self.actionButton.isSelected ? UIColor.red : .white
    }
}
